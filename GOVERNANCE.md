# Governance

### Roles Overview
In order of descending power:

- **Benevolent Dictator For Life\***:

  Representative of the Project as a whole. 

  Coordinates progress on the project as a whole. Has the power to veto certain votes in special cases.

  \* Unless voted out by a unanimous Core Team vote, so really Benevolent Dictator Until Assassinated

> See [BDFL.md](governance/BDFL.md)

- **Core Team Member**:

  A trusted individual who is usually the first point of call for big decisions, and helps coordinate teams.

> See [CORE_TEAM.md](governance/CORE_TEAM.md)

- **Team Lead**:

  Coordinates a specific team and/or focus on the Project. (e.g., the Web Team Lead would manage progress on the website)

  Provides general direction and substance to their respective team.

> See [TEAM_LEAD.md](governance/TEAM_LEAD.md)

- **Trusted Contributor**:

  A trusted individual who has made multiple contributions to the project (not including financial support).

> See [TRUSTED_CONTRIBUTOR.md](governance/TRUSTED_CONTRIBUTOR.md)

- **Team Member**:

  A **Contributor** that has a focus on a general area of the project. (e.g., someone passionate about Rust could be a member of the Rust Team)

  Generally contributes to their specific team. (But can, of course, contribute to others)

> See [TEAM_MEMBER.md](governance/TEAM_MEMBER.md)

- **Contributor**:

  A role granted to members of the community that in general have shown a passion to or have already Contributed to the Project, be this through Design, Code, or similar.

> See [CONTRIBUTOR.md](governance/CONTRIBUTOR.md)

### By-Laws

Each of the documents linked above defines **Responsibilities**, **Powers** and rules regarding **Appointment/Dismissal** of each role.

These are considered finalized, however, amendments can be made as follows:

#### Amending the By-Laws

By-Laws can be amended through two possible processes:

- An RFC can be opened, regarding the change of one or more parts of the By-Laws.

  This would constitute a **Major vote**.

- Under exceptional circumstances, the Project Lead can act reasonably to remedy the situation, as well as request for the **Core Team** to amend the by-laws so that they cover the situation in the future.
    
  This constitutes part of the **Project Lead**s power to veto certain actions.

Once the amendment is decided on and enacted, it is important to **ensure that an RFC is opened**, so that changes to the By-Laws over time are tracked.

### Finances

Finances are managed on OpenCollective by the Open Source Collective fiscal host. Internally, however, a new expense (or the removal of a stale one) must be opened as an RFC.

This constitutes a **Major vote**.

---

### Glossary

- **Major vote**:
  
  A vote requiring at least 75%+ consensus to enact. The relevant By-Laws cover which person(s) may be able to veto a vote or break a tie.

- **Minor vote**:

  A vote requiring at least 50%+ consensus to enact. The relevant By-Laws cover which person(s) may be able to veto a vote or break a tie.

- **RFC**:

  Request for comment. A special kind of proposal that affects either the Project as a whole or greatly affects any subset of the project. 

  Submitted as an issue template on the `crystal/info` repository, these can constitute either **Major vote** or a **Minor vote**, whichever is fit for the current context.

- **Veto**:

  The power to unilaterally (without possible objection) deny a decision. Should not be used lightly.
