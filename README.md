# 🔮 Crystal Linux

Welcome to the Crystal Linux GitLab!

Crystal Linux is a distribution focused on providing the best of **performance**, **accessibility**, and **usability**. We base ourselves off [Arch Linux](https://archlinux.org) and use mainly [GNOME](https://gnome.org) technologies (e.g., GTK4/Libadwaita, GNOME Desktop)

Check out our socials below!

---

### Socials
- [Discord Server](https://getcryst.al/discord)
- [Forum](https://forum.getcryst.al)
- [Mastdon](https://fosstodon.org/@crystal_linux)
- [Twitter](https://twitter.com/Crystal_Linux)

Remember that our [Code of Conduct](CODE_OF_CONDUCT.md) applies in all of these facets of the community!

---

### Quick Links

Here are some quick links that detail some important information:

- [Code of Conduct](CODE_OF_CONDUCT.md)
- [Governance](GOVERNANCE.md)
- [License](LICENSE)
- [Contributing](CONTRIBUTING.md)

---

### Structure

Here's our organisational structure, for more info on governance click [here](GOVERNANCE.md)!

| Team | Team Name | Team Lead |
| -----| --------- | --------- |
| @crystal-linux | 🔮 Core Team | @mchal_         |
| @crystal-linux/teams/trusted-contributors | 💜 Trusted Contributors | @crystal-linux (Core Team) |
| @crystal-linux/teams/rust | 🦀 Rust | @trivernis |
| @crystal-linux/teams/python | 🐍 Python | @axtloss2 |
| @crystal-linux/teams/design | 🎨 Design | @hericiumvevo |
| @crystal-linux/teams/distribution | 📦 Distribution | @SomethingGeneric |
| @crystal-linux/teams/infrastructure | 🦴 Infrastructure | @SomethingGeneric |
| @crystal-linux/teams/web | 🕸️ Web | ***EMPTY.*** Could be you? 👀 |
<!-- | @crystal-linux/teams/i18n-l10n-a11y | 👥 i18n l10n and a11y | @mchal_ | -->
| @crystal-linux/teams/mirrors | Mirrors | @relms |
