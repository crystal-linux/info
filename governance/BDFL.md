# Regarding the Benevolent Dictator For Life*

## Powers

* The ultimate say in any RFCs, approvals, or dismissals
  This is naturally **not** to be taken lightly, and a vote by Trusted Contributors would still be the main way for an RFC to pass.

## Responsibilities

* Coordinate between teams with assistance of Core Team
* Represent the project as needed to outside organizations
  This also involves taking liability/responsibility for the project in billing matters

## Appointment/Dismissal

* No term limit, may resign/step down at any time
* Removed by vote of Core Team (unanimous vote)