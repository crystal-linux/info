# Regarding Trusted Contributors

## Powers

* Vote on RFCs (minor vote, unless a consensus agrees)

## Benefits

* `getcryst.al` email and LDAP for services hosted by the project

## Appointment/Dismissal

* First Trusted Contributor appointments by Core Team. Subsequent appointments are a minor vote by Core Team
* Removed by minor vote of Core Team

## Considered MIA
If a trusted contributor has not shown any sign of life (defined below) in a given two month period, then the other TC's (or, the BDFL via their absolute powers), shall vote on considering the given TC to be MIA.

If the vote passes, their password will be reset, 2fa will be reset, and existing sessions of their account (where possible) will be terminated for security reasons. 

If this process occurs, the TC is considered MIA for the next two months, before all account data and access will be permanently removed, if no signs of life occur. Before account removal, the TC's and/or BDFL must re-confirm that they have seen no signs of life from the affected TC

Signs of life are defined as: Messages in discord (either DM to other TC's, or activity in the Crystal Linux channel), messages in the forum, or other platforms to members of the community and/or other TC's. Additional signs of life could include GitLab activity (but ONLY if the activity relates to the project).
