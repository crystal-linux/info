```md
Before you submit your RFC, please ensure all fields are filled out to the best of your ability.
Make sure you remove this note before submitting!
Also, please title your RFC in the following format: "RFC: Foo Bar" :)
```

## Summary
> Describe your RFC in a few words

## Date Proposed
> Today's date in YYYY-MM-DD format.

## Benefits
> How would enacting this RFC benefit the project?

## Drawbacks
> Are there any drawbacks to enacting this RFC?

## Tasks
> What steps need to be taken to implement this RFC?

## Alternatives
> Are there any alternative options to consider?
