# Contributing Guidelines
By contributing to Crystal Linux, you agree to the following:

You understand and agree to:
- The terms of the [DCO](https://developercertificate.org/) for all of your contributions to Crystal Linux
- The terms of the [GPLv3.0-only](LICENSE) license for all of your contributions to Crystal Linux
- The Crystal Linux [Code of Conduct](CODE_OF_CONDUCT.md), and understand that you are expected to follow it at all times

As well as the following code standards:

#### General
- Use **git tags** to denote package releases ready to be packaged by @crystal/teams/distribution
- Develop on **separate, feature branches** that you merge into main
- Write useful, imperative commit messages
- Split up your commits in a smart way

#### Rust
Always ensure your code passes:
- `cargo fmt --check`
- `cargo clippy --all -- -D clippy::all`

#### Python
Always ensure your code passes:
- `black . --check`

